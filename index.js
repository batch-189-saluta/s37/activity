const express = require('express');
const mongoose = require('mongoose');
// cors - Allows our backend application to be available to our frontend application
const cors = require('cors');
const userRoutes = require('./routes/userRoutes.js')
const courseRoutes = require('./routes/courseRoutes.js')
const port = 4000

const app = express()

// express setup----------------------------------\
app.use(cors()) //allows all resources to access our backend application
app.use(express.json())
app.use(express.urlencoded({extended: true}))



// mongoose setup------------------------------------
mongoose.connect('mongodb+srv://admin123:admin123@zuitt-bootcamp.rgocvci.mongodb.net/s37-s41?retryWrites=true&w=majority',{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', ()=> console.error.bind(console, 'error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));


// routes------------------------------------------------------
app.use('/users', userRoutes)
app.use('/courses', courseRoutes)


// listen-------------------------------------------------------
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
})