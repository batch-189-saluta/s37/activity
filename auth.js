const jwt = require('jsonwebtoken')

const secret = "CourseBookingAPI"


module.exports.createAccessToken = (user) => {

	// payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {}) //(<payload>, <secret key>, {options})
}

module.exports.verify = (req, res, next) => {

	// console.log(req.headers)
	let token = req.headers.authorization;

	if(typeof token !== "undefined"){

		// console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) {
				return res.send({auth: "failed"})
			} else {
				next()
			}
		})
	} else {

		return res.send({auth: "failed"})
	}
}


// decode------------------------------------------------------
module.exports.decode = (token) => {

	if(typeof token !== "undefined") {

		token = token.slice(7, token.length) 

		return jwt.verify(token, secret, (err, data) => {

			if(err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})

	} else {
		return null
	}
}