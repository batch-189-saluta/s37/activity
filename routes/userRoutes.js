const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController.js')
const auth = require('../auth.js')

router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
})


// registration route---------------------------
router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


// login route---------------------------
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


// single detail route--------------------------------------
router.get('/details', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	// const isAdminData = auth.decode(req.headers.authorization).isAdmin;
	// console.log(isAdminData)
	//console.log(userData)
	/*
		{
		  id: '62c2ac569c1bbbf99fb09a87',
		  email: 'fjsaluta@mail.com',
		  isAdmin: false,
		  iat: 1656985821
		}

	*/
	//console.log(req.headers.authorization)

	userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController))
})


//  Enroll----------------------------------------------

router.post('/enroll', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})


module.exports = router;