const express = require('express')
const router = express.Router()
const auth = require('../auth.js')

const courseController = require('../controllers/courseController')

// route for creating a course---------------------------------------
router.post('/', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	// console.log(userData)

	if(userData.isAdmin == true) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('failed: Authorized admin only')
	}

	
})


// Route for retrieving all the courses---------------------------------
router.get('/all', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
})


// Routes for retrieving all active courses------------------------------

router.get('/', (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})


// Route for retrieving a specific course-------------------------------

router.get('/:courseId/', (req, res) => {

	console.log(req.params)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})


// Route for updating a course-------------------------------------------

router.put('/:courseId', auth.verify, (req,res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})


// route for deactivation of course-------------------------------------

router.put('/:courseId/archive', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) {
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Failed: Authorized admin only')
	}
})

module.exports = router