const Course = require('../models/Course')

// Create new course--------------------------------------
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// Retrieve all Courses-------------------------------------------

module.exports.getAllCourses = async (data) => {

	if (data.isAdmin) {
		return Course.find({}).then(result => {
			return result
		})
	} else {

		return "Not an Admin"
	}
}

// ---------------------------------------------

module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {

		return result
	})
}

// retriving a specific course--------------------------------------------------------

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result
	})
}


// Updating a course------------------------------------------

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
	
}

// archive a course----------------------------------------------

module.exports.archiveCourse = (reqParams, reqBody) => {

	let archivedCourse = {
		isActive: reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		
		if (error){
			return false
		} else {
			return true
		}
	})
}